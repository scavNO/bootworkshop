# Spring Boot Workshop #

*Spring Boot Workshop template project*

You are given a **GRADLE** ready **Spring Boot** project which will run embedded Tomcat within your IDE.
You are also given a basic unit test for a rest controller. 

There is no need to provide a database, the application will use everything in the package DOMAIN to provide
you with a working H2 embedded development database. 

On the classpath you will also find a file named **classpath:import.sql**. This has nothing to do with Spring, but is a
Hibernate import file for SQL data. It contains randomly generated PRODUCER entities.

## The objective of the workshop ##
1. Implement the repository and services needed to:
    - List one PRODUCER
    - List all PRODUCERS
    - Save a new PRODUCER
    - Delete a PRODUCER
    
2. Write Unit tests to make sure that the service works. You do not need to write tests for the
repository as this is tested extensively by the Spring team.

3. Implement controllers in the CONTROLLER package.
    - A REST controller listing PRODUCER by id
    - A REST controller listing all PRODUCERS
    - A view and form based controller for adding, deleting and listing PRODUCER from **classpath:templates/producer.html**
    

Implement a form based controller to enable both posting and fetching of data from Tomcat and the application.

## Hints ##
When implementing the repository, try extending it with **CrudRepository<Producer, Long>** to make sure that your repository only
fetches producers and used Long for the magic.

**@Autowired** will give you access to all **@Service**, **@Component** and **@Bean** classes. Use it to fetch your repository from the service.

Implement the provided Service interfaces and annotate the implementation with **@Service("producerService")**


