package no.nsd.bootworkshop.repository;


import no.nsd.bootworkshop.domain.Producer;
import org.springframework.data.repository.CrudRepository;

public interface ProducerRepository extends CrudRepository<Producer, Long> {
}
