package no.nsd.bootworkshop.controller;

import no.nsd.bootworkshop.domain.Producer;
import no.nsd.bootworkshop.service.ProducerService;
import no.nsd.bootworkshop.utils.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Locale;

@Controller
@RequestMapping("producer")
public class ProducerController {

    @Autowired
    private ProducerService producerService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String form(Model model, final Producer producer) {

        model.addAttribute("producers", producerService.findAll());

        return "producer";
    }

    @RequestMapping(value= "", method = RequestMethod.POST)
    public String save(@Valid final Producer producer, final BindingResult bindingResult,
                       final ModelMap model, Locale locale) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("message", new Message("message-save-producer-form",
                    messageSource.getMessage("message_save_producer_failed",
                            new Object[]{},
                            locale)));

            model.addAttribute("producers", producerService.findAll());
            model.addAttribute("producer", producer);

            return "producer";
        }

        this.producerService.save(producer);
        model.clear();

        model.addAttribute("message", new Message("message-save-producer-form",
                messageSource.getMessage("message_save_producer_success",
                        new Object[]{},
                        locale)));

        return "redirect:/producer";
    }

    @RequestMapping(value = "delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        producerService.delete(producerService.findById(id));

        return "redirect:/producer";
    }
}
