package no.nsd.bootworkshop.controller;

import no.nsd.bootworkshop.domain.Producer;
import no.nsd.bootworkshop.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("rest/producer/")
public class ProducerRestController {

    @Autowired
    private ProducerService producerService;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Producer> getAll() {
        return producerService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Producer getOne(@PathVariable("id") Long id){
        return producerService.findById(id);
    }
}
