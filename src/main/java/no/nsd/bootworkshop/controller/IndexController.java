package no.nsd.bootworkshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.Instant;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("")
    public String index(Model model) {
        String dateTime = Instant.now().toString();
        model.addAttribute("date", dateTime);

        return "index";
    }

    @RequestMapping("login")
    public String login(Model model) {
        String dateTime = Instant.now().toString();
        model.addAttribute("date", dateTime);

        return "login";
    }
}