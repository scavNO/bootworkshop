package no.nsd.bootworkshop.domain;

import java.time.Instant;
import java.util.Date;

public class TimePrd {

    private Date start;

    private Date end;

    public TimePrd(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public TimePrd() {
        this.start = Date.from(Instant.now());
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
