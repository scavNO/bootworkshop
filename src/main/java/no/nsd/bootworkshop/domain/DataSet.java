package no.nsd.bootworkshop.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "dataset")
public abstract class DataSet {

    @Id
    @Column(name = "meal_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private DataKind dataKind;

    private TimePrd timePrd;

    private String universe;

    private Long sampProc;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "user_dataset",
            joinColumns = {@JoinColumn(name = "dataset_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> dataCollector;

    //
    private Producer affiliation;

}
