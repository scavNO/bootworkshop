package no.nsd.bootworkshop.domain;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.Set;

public class User {

    private Long id;

    private String firstName;

    private String surName;

    private String username;

    private String password;

//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private Producer institution;
//
//    @ManyToMany(mappedBy = "ingredients", fetch = FetchType.LAZY)
//    private Set<DataSet> dataSet;


    public User(Long id, String firstName, String surName, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return !(firstName != null ? !firstName.equals(user.firstName) : user.firstName != null)
                && !(id != null ? !id.equals(user.id) : user.id != null)
                && password.equals(user.password)
                && !(surName != null ? !surName.equals(user.surName) : user.surName != null)
                && !(username != null ? !username.equals(user.username) : user.username != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (surName != null ? surName.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", surName='" + surName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
