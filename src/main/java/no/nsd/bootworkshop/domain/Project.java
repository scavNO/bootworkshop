package no.nsd.bootworkshop.domain;

public class Project {

    private String grantNo;

    private String titl;

    private Producer producer;

    private AuthEnty authEnty;

    private TimePrd timePrd;

    private FundAg fundAg;

    private String txtAbstract;

    public Project(String grantNo, String titl, Producer producer, AuthEnty authEnty, TimePrd timePrd, FundAg fundAg, String txtAbstract) {
        this.grantNo = grantNo;
        this.titl = titl;
        this.producer = producer;
        this.authEnty = authEnty;
        this.timePrd = timePrd;
        this.fundAg = fundAg;
        this.txtAbstract = txtAbstract;
    }

    public Project() {
    }

    public String getGrantNo() {
        return grantNo;
    }

    public void setGrantNo(String grantNo) {
        this.grantNo = grantNo;
    }

    public String getTitl() {
        return titl;
    }

    public void setTitl(String titl) {
        this.titl = titl;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public AuthEnty getAuthEnty() {
        return authEnty;
    }

    public void setAuthEnty(AuthEnty authEnty) {
        this.authEnty = authEnty;
    }

    public TimePrd getTimePrd() {
        return timePrd;
    }

    public void setTimePrd(TimePrd timePrd) {
        this.timePrd = timePrd;
    }

    public FundAg getFundAg() {
        return fundAg;
    }

    public void setFundAg(FundAg fundAg) {
        this.fundAg = fundAg;
    }

    public String getTxtAbstract() {
        return txtAbstract;
    }

    public void setTxtAbstract(String txtAbstract) {
        this.txtAbstract = txtAbstract;
    }
}
