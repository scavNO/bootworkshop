package no.nsd.bootworkshop.domain;

public enum DataKind {
    INTERVIEW,
    REGISTER,
    TEXT,
    CLINICAL_DATA,
    EXPERIMENT,
    PICTURE,
    AUDIO,
    MEASUREMENT,
    REGISTRATIONS,
    OTHER
}
