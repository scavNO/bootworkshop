package no.nsd.bootworkshop.utils;

public class Message {

    private String type;
    private String text;

    /**
     * Empty default constructor. Should not be called.
     */
    public Message() {

    }

    /**
     * Constructor for actual messages. Should be used when ever
     * a proper message will be sent across the system.
     * @param messageType
     * @param text
     */
    public Message(String messageType, String text) {
        this.type = messageType;
        this.text = text;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Set the text message, including a JQuery snippet to remove
     * it after a certain time.
     *
     * @param text
     */
    public void setText(String text) {
        this.text += text;
    }
}
