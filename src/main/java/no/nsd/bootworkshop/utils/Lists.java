package no.nsd.bootworkshop.utils;

import java.util.ArrayList;
import java.util.List;

public class Lists<T> {

    /**
     * Produce an {@link java.util.ArrayList} from a {@link java.lang.Iterable}
     * @param iterable from anywhere.
     * @return an {@link java.util.ArrayList} representation of a {@link java.lang.Iterable}
     */
    public List<T> iterableToList(Iterable<T> iterable) {
        List<T> aList = new ArrayList<>();
        iterable.forEach(aList::add);
        return aList;
    }
}