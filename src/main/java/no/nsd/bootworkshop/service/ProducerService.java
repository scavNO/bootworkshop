package no.nsd.bootworkshop.service;

import no.nsd.bootworkshop.domain.Producer;

import java.util.List;

public interface ProducerService {

    public Producer findById(Long id);

    public List<Producer> findAll();

    public Producer save(Producer producer);

    public void delete(Producer producer);
}