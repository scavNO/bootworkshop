package no.nsd.bootworkshop.service;

import no.nsd.bootworkshop.domain.Producer;
import no.nsd.bootworkshop.repository.ProducerRepository;
import no.nsd.bootworkshop.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("producerService")
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerRepository producerRepository;

    private Lists<Producer> list = new Lists<>();

    @Override
    public Producer findById(Long id) {
        return producerRepository.findOne(id);
    }

    @Override
    public List<Producer> findAll() {
        return list.iterableToList(producerRepository.findAll());
    }

    @Override
    public Producer save(Producer producer) {
        return producerRepository.save(producer);
    }

    @Override
    public void delete(Producer producer) {
        producerRepository.delete(producer);
    }
}
