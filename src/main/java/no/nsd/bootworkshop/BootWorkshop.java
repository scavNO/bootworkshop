package no.nsd.bootworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class BootWorkshop extends SpringBootServletInitializer {

    /**
     * Create the Application
     * @param applicationBuilder spring application builder
     * @return the sources used to build the application
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder) {
        return applicationBuilder.sources(BootWorkshop.class);

    }

    /**
     * Main method used to bootstrap the Spring Boot Jar.
     * @param args
     */
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(BootWorkshop.class);
    }

    /**
     * A Bean to provide access Spring's {@link org.springframework.http.converter.json.MappingJackson2HttpMessageConverter}     *
     * @return {@link org.springframework.http.converter.json.MappingJackson2HttpMessageConverter} with human readable json.
     */
    @Bean
    public MappingJackson2HttpMessageConverter messageConverter() {
        MappingJackson2HttpMessageConverter messageConverter =
                new MappingJackson2HttpMessageConverter();

        messageConverter.setPrettyPrint(true);

        return messageConverter;
    }
}
