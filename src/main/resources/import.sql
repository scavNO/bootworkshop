-- These are our import statements used by the H2 database during development.
-- Must never be used in producion.

INSERT INTO producer (name, email) VALUES('Carlos', 'car73@example.com');
INSERT INTO producer (name, email) VALUES('Eleonora', 'ele2@example.com');
INSERT INTO producer (name, email) VALUES('Leana', 'lea32@example.com');
INSERT INTO producer (name, email) VALUES('Marcos', 'mar87@example.com');
INSERT INTO producer (name, email) VALUES('Alison', 'ali24@example.com');
INSERT INTO producer (name, email) VALUES('Keshia', 'kes98@example.com');
INSERT INTO producer (name, email) VALUES('Russ', 'rus18@example.com');
INSERT INTO producer (name, email) VALUES('Tiffiny', 'tif21@example.com');
INSERT INTO producer (name, email) VALUES('Yuette', 'yue47@example.com');
INSERT INTO producer (name, email) VALUES('Beverlee', '54bev@example.com');
INSERT INTO producer (name, email) VALUES('Sheryll', '31she@example.com');
INSERT INTO producer (name, email) VALUES('Tish', 'tis48@example.com');
INSERT INTO producer (name, email) VALUES('Alisha', 'ali12@example.com');
INSERT INTO producer (name, email) VALUES('Marisol', 'mar98@example.com');
INSERT INTO producer (name, email) VALUES('Hiedi', 'hei77@example.com');
INSERT INTO producer (name, email) VALUES('Nedra', 'ned65@example.com');
INSERT INTO producer (name, email) VALUES('Barbera', 'bar43@example.com');
INSERT INTO producer (name, email) VALUES('Elma', 'elm82@example.com');
INSERT INTO producer (name, email) VALUES('Dario', 'dar1@example.com');
INSERT INTO producer (name, email) VALUES('Agatha', 'aga2@example.com');
INSERT INTO producer (name, email) VALUES('Inga', 'ing66@example.com');
INSERT INTO producer (name, email) VALUES('Adell', 'ade21@example.com');
INSERT INTO producer (name, email) VALUES('Parthenia', 'par39@example.com');
INSERT INTO producer (name, email) VALUES('Jerrell', 'jar76@example.com');
INSERT INTO producer (name, email) VALUES('Meri', '77mer@example.com');
INSERT INTO producer (name, email) VALUES('Inger', 'ing00@example.com');
INSERT INTO producer (name, email) VALUES('Chi', 'chi22@example.com');
INSERT INTO producer (name, email) VALUES('Kirsten', 'kir51@example.com');
INSERT INTO producer (name, email) VALUES('Preston', 'pre91@example.com');
INSERT INTO producer (name, email) VALUES('Adolfo ', '97ado@example.com');