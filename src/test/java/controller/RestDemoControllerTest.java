package controller;

import no.nsd.bootworkshop.BootWorkshop;
import no.nsd.bootworkshop.domain.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@IntegrationTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BootWorkshop.class)
public class RestDemoControllerTest {

    @Test
    public void testProducers() throws Exception {
        RestTemplate template = new RestTemplate();
        ResponseEntity<Producer[]> producersEntity = template.getForEntity(
                "http://localhost:8080/rest/producer/list", Producer[].class);

        Producer[] producers = producersEntity.getBody();

        assertThat(producersEntity.getStatusCode().toString(), is("200"));
        for(Producer producer : producers) {
            assertNotNull(producer);
        }
    }
}